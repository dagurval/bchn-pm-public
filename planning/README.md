Backporting file notes and explanations
=======================================

The `bchn-backporting.csv` is a database we use to track backports.
It is operated on both by tools to pull in new potential backports
based on commits in Bitcoin ABC and Bitcoin Core repositories), as
well as manually (updating with management decisions and automatically
marking as 'integrated' those backports for which merge commits on
Bitcoin Cash Node are detected.

The CSV file is in Windows line ending format (CRLF) and that should
be preserved by any edits. Please make sure your text editor or whatever
other tool you use to operate on it, is set to preserve both the line
ending style, as well as the general formatting style of the CSV (see Formatting Rules)


Formatting rules
----------------

- keep CRLF line end format!
- Comma separated values.
- Fields containing values with commas in them must be double quoted.
Particularly watch out for this when adding comments (last column).
- no extra spaces in comma-separated lists of Diffs/PRs in dependencies field
- References to BCHN MRs: !xyz
- References to Diffs:  Dnnnn
- References to Core PRs: PR#mmmmm
- No spaces in comma separated lists like "D1234,D2456"


Do
--

- Edit the status
- Enter an associated code MR with an entry once it exists
- Add manual dependencies which are not explicit in the upstream commits and hence cannot be picked up by our automation (this happens)
- Always add a Rejection reason or Deferral reason when a backport is decided to be rejected or deferred (postponed)
- Add comments (last column) you think may be helpful to other users of the file when considering a particular entry


Do not
------

- Edit the first field (internal identifier maintained only by tools)
- (under most circumstances) Edit the dependency field - it is maintained by tools
- Change the Diff or PR association of an existing entry


Explanation of the fields
-------------------------

- bp_id: an internal ID for us to refer to some tracked backport issue.
- bp_status: status is one of the ones described in the next section below
- mr: an identifier such as !1 referring to a BCHN client Merge Request
- abc_diff: an identifier such as D1234 referring to an ABC Diffusion (commit) number
- core_pr: an identifier such as PR#98765 referring to a Core Github Pull Request number
- depends: dependency information such as a comma-separated list of ABC Diffs. Use quotes if comma separated list.
- upstream_commit: the SHA1 hash of the upstream commit (Diff or Core merge commit)
- commit_desc: brief description of the upstream commit (= commit title + author)
- comment:  BCHN internal comment on this backport. Free form, but obey CSV rules (ie. quote if have commas etc)

Backport status field values
----------------------------

- unevaluated: no-one has looked deep enough at this to make a call
- deferred: has been looked at, and decision is to re-evaluate later
- rejected: this change has been rejected (not needed, or bad, etc)
- planned: is planned for integration (this DB does not track schedule - that is for taskjuggler to do)
- integrated: change has been integration, no further action needed at this point
- reverted: change has been integrated, and since reverted. Should go to deferred or planned if the idea is to re-evaluate or re-integrate later.

When a backport is rejected, deferred or reverted, some note should be added in the comments about the reason.
